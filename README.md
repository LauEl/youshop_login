#    LauEl / 萤火虫商城系统兼容2021年4.13发布的微信小程序登录接口插件


#### 安装教程

1. 覆盖源码
2. 前端重提交审核
3. 版本库一定要2.13+

修改前：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0527/154046_68caf2f8_902699.png "屏幕截图.png")


修改后：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0527/154056_7586abbc_902699.png "屏幕截图.png")