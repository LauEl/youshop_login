const App = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    authorized: true,
    options: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _this = this;
    _this.setData({
      authorized : false,
      options
    });
  },

  /**
   * 授权登录
   */
  getUserInfo(e) {
    let _this = this;
    App.getUserInfo(e, function(result){
      console.log(result)
      if (result.data.phone == '')
      {
        _this.setData({
          authorized: !_this.data.authorized,
        })
      }else{
        _this.onNavigateBack(1);
      }
    });
  },

/**
   * 授权登录（新版）
   */
  getUserProfile() {
    const app = this
    let _this = this;
    wx.canIUse('getUserProfile') && wx.getUserProfile({
      lang: 'zh_CN',
      desc: '获取用户相关信息',
      success({
        userInfo
      }) {
        console.log('用户同意了授权')
        console.log('userInfo：', userInfo)
        // App.getUserInfo(userInfo, () => {
          // 跳转回原页面
          // app.onNavigateBack(1)
        // })
        App.getUserInfo(userInfo, function(result){
          console.log(result);
          if (result.data.phone == '')
          {
            _this.setData({
              authorized: !_this.data.authorized,
            })
          }else{
            _this.onNavigateBack(1);
          }
        });
      },
      fail() {
        console.log('用户拒绝了授权')
      }
    })
  },

  /**
   * 绑定手机号
   * */ 
  getPhoneNumber(e){
    var _this = this;

    // 执行微信登录
    wx.login({
      success(res) {
        // 发送用户信息
        App._post_form('user/bindphone', {
          code: res.code,
          encrypted_data: e.detail.encryptedData,
          iv: e.detail.iv
        }, result => {
          // 跳转回原页面
          _this.onNavigateBack(1);
        }, false, () => {
          wx.hideLoading();
        });
      }
    });
  },

  /**
   * 暂不登录
   */
  onNotLogin() {
    let _this = this;
    // 跳转回原页面
    _this.onNavigateBack(_this.data.options.delta);
  },

  /**
   * 授权成功 跳转回原页面
   */
  onNavigateBack(delta) {
    wx.navigateBack({
      delta: Number(delta || 1)
    });
  },

})